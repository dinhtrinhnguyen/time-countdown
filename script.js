const second = 1000,
minute = second * 60,
hour = minute * 60,
day = hour * 24;

// let countDown = new Date('Jan 1, 2021 00:00:00').getTime(),
let countDown = new Date('Sep 24, 2020 00:41:00').getTime(),
x = setInterval(function() {    

  let now = new Date().getTime(),
  distance = countDown - now;

  let days = Math.floor(distance / (day)).toString(),
  hours = Math.floor((distance % (day)) / (hour)).toString(),
  minutes = Math.floor((distance % (hour)) / (minute)).toString(),
  seconds = Math.floor((distance % (minute)) / second).toString();

  // days = (days >= 10) ? days : ('0' + days.toString());
  // hours = (hours >= 10) ? hours : ('0' + hours.toString());
  // minutes = (minutes >= 10) ? minutes : ('0' + minutes.toString());
  // seconds = (seconds >= 10) ? seconds : ('0' + seconds.toString());


  document.getElementById('days').innerText = (days > 9) ? days : ('0'+ days.toString()),
  document.getElementById('hours').innerText = (hours > 9) ? hours : ('0' + hours.toString()),
  document.getElementById('minutes').innerText = (minutes >= 9) ? minutes : ('0' + minutes.toString()),
  document.getElementById('seconds').innerText = (seconds > 9) ? seconds : ('0' + seconds.toString());

  if (distance <= 0) {
    clearInterval(x);
    document.alert("Happy new Year 2021");
  }

}, second)